const { sequelize } = require("..");
const { Sequelize } = require("sequelize");
const Mark = require("./mark.model");

class Record extends Sequelize.Model {}

Student.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    date: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    teacherName: {
      type: Sequelize.DataTypes.UUID,
    },
    mark: {
      type: Sequelize.INTEGER,
    }
  },
  { sequelize: sequelize, modelName: "recordBook" }
);

module.exports = Record;
