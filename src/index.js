const { initDB } = require("./database/config");
const Record = require("./database/models/record.model");
const fs = require("fs");

async function bootstrap() {
  await initDB();
  const recordDateList = await Record.findAll({
    attributes: ["id", "date"],
  });

  fs.writeFileSync(
    "./output.json",
    JSON.stringify({
      recordDateList,
    })
  );
}

bootstrap();
